package classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class AlunoBean_2 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String nome;
    private String sobre;
    private Boolean aceito;

    public void cadastrar() {
        System.out.println("Nome:"+ this.nome);
        System.out.println("Sobre: " + this.sobre);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Cadastro realizado!")); //joga na tela a janela informando o cadastro
    }

    public List<String> completarTexto(String consulta) {
        List<String> resultados = new ArrayList<>();

        if (consulta.startsWith("T")) {
            resultados.add("Thiago");
            resultados.add("Tânia");
            resultados.add("Tiago");
            resultados.add("Teodora");
        }

        return resultados;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobre() {
        return sobre;
    }

    public void setSobre(String sobre) {
        this.sobre = sobre;
    }

}
