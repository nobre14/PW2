
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Produto> produtos = new ArrayList<>();
	private Produto produto;

        @PostConstruct // Carrega primeiro , garantindo que todas variáveis estão preenchidas para a tabela
        public void init(){
            produtos.add(new Produto("nome1", 100.0));
            produtos.add(new Produto("nome2", 200.0));
            produtos.add(new Produto("nome3", 300.0));
        }
	/*public void incluirProduto() {
		produtos.add(nomeProduto);
		nomeProduto = null;
	}*/
	
	public Produto getNomeProduto() {
		return produto;
	}

	public void setNomeProduto(Produto produto) {
		this.produto = produto;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}
        public void editar(){
            
        }
        
        public void excluir(){
            produtos.remove(this.produto);
        }

}